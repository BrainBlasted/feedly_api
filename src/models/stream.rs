use super::entry::{Link, Entry};

#[derive(Debug, Deserialize)]
pub struct Stream {
    pub id: String,
    #[serde(default)]
    pub title: Option<String>,
    #[serde(default)]
    pub direction: Option<String>,
    #[serde(default)]
    pub updated: Option<u64>,
    #[serde(default)]
    pub alternate: Option<Vec<Link>>,
    #[serde(default)]
    pub continuation: Option<String>,
    pub items: Vec<Entry>,
}
use super::super::RefreshToken;
use super::super::AccessToken;

#[derive(Debug, Serialize, Deserialize)]
pub struct AccessTokenResponse {
    pub id: String,
    pub refresh_token: RefreshToken,
    pub access_token: AccessToken,
    pub expires_in: u32,
    pub token_type: String,
    pub plan: String,
    pub provider: String,
    #[serde(default)]
    pub state: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct RefreshTokenResponse {
    pub id: String,
    pub refresh_token: RefreshToken,
    pub access_token: AccessToken,
    pub expires_in: u32,
    pub token_type: String,
    pub plan: String,
}
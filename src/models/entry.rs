use super::Tag;
use super::Category;

#[derive(Debug, Serialize, Deserialize)]
pub struct Entry {
    pub id: String,
    #[serde(default)]
    pub title: Option<String>,
    #[serde(default)]
    pub content: Option<Content>,
    #[serde(default)]
    pub summary: Option<Content>,
    #[serde(default)]
    pub author: Option<String>,
    pub crawled: u64,
    #[serde(default)]
    pub recrawled: Option<u64>,
    pub published: u64,
    #[serde(default)]
    pub updated: u64,
    #[serde(default)]
    pub alternate: Option<Vec<Link>>,
    #[serde(default)]
    pub origin: Option<Origin>,
    #[serde(default)]
    pub keywords: Option<Vec<String>>,
    #[serde(default)]
    pub visual: Option<Visual>,
    pub unread: bool,
    #[serde(default)]
    pub tags: Option<Vec<Tag>>,
    #[serde(default)]
    pub categories: Option<Vec<Category>>,
    #[serde(default)]
    pub engagement: Option<i32>,
    #[serde(default)]
    #[serde(rename = "actionTimestamp")]
    pub action_timestamp: Option<i32>,
    #[serde(default)]
    pub enclosure: Option<Vec<Link>>,
    pub fingerprint: String,
    #[serde(rename = "originId")]
    pub origin_id: String,
    #[serde(default)]
    pub sid: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Content {
    pub content: String,
    #[serde(default)]
    pub direction: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Link {
    pub href: String,
    #[serde(default)]
    #[serde(rename = "type")]
    pub _type: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Origin {
    #[serde(rename = "streamId")]
    pub stream_id: String,
    pub title: String,
    #[serde(rename = "htmlUrl")]
    pub html_url: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Visual {
    pub url: String,
    #[serde(default)]
    pub width: Option<u32>,
    #[serde(default)]
    pub height: Option<u32>,
    #[serde(default)]
    #[serde(rename = "contentType")]
    pub content_type: Option<String>,
}